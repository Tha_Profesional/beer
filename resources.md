# Resources
[Grain substitutions](https://www.brew.is/files/malt.html)  
[Priming sugar calculator](https://www.brewersfriend.com/beer-priming-calculator/)  
To convert between LME, DME and grains use:  

- [This](https://www.homebrewersassociation.org/how-to-brew/3-simple-steps-converting-grain-recipes-extract/) or [this](https://www.jaysbrewing.com/2011/11/17/lazy-chart-for-converting-dme-lme-grain/)  
