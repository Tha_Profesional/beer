# Harold?!

### Grains
480g German Pilsner  
480g Wheat malt

### Water
4.5l end
8.1l pre-boil  

- 4.5l \+ 1.06l grain absorbtion \+ 2.51l evaporation

### Mash
6l at 152f for an hour

### Sparge
2l at 170f for 15 minutes

### Hops
4g Hallertauer

### Boil
90m total  
hops at 60m to go

### Goal OG
1.050

### Yeast
1/5 packet (2.5g) SAF Wheat yeast WB-06 11.5g

### Goal pitch temp
20c or under

### Primary
17c for first few days

### Goal FG
1.012

### Secondary
3.8 volumes

### Source
Brewing Classic Styles - Jamil Zainasheff, John Palmer

### Notes
- Scaled down from 22.7 litres to 4.5l


## History

### 2021-01-02
#### Brewing / fermentation:
- Came in 450ml under, so added water before measuring gravity
	- added 1 litre more to pre-boil
- 1.042g
	- upped both grains 50g to try get in range
- Realised I mashed too high (at 162f)
	- lowered to 152f as per recipe
	- check how the taste goes.. if it's too thick (from the warmer mash) or just right
- Chucked in a bucket full of water, cycled ice packs into water once a day
	- this turned out to be too much first few days - fermentation seemed to cut off
	- emptied water after a few days, then on hot days half-filled the bucket
