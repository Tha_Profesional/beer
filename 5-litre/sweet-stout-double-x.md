# Double-X

### Grains
900g Marris Otter Malts  
89kg black malt  
67g Dark crystal  
45g chocolate malt

### Water
4.5l total  
7l pre-boil  

- 4.5l \+ 1.21l grain absorption \+ 1.29l (18%) evaporation

### Mash
5l at 151f for 60m

### Sparge
2l at 170f for 15m

### Hops
8.5g Goldings

### Other ingredients
89g lactose powder

### Boil
60m  
hops at 60m  
lactose at 60m

### Goal OG
1.054

### Yeast
1/5 packet (2.5g) Safale S-04

### Goal pitch temp
20c

### Primary
20c

### Goal FG
1.023

### Secondary
1.8 volumes

### Source
Brewing Classic Styles - Jamil Zainasheff, John Palmer

### Notes
- Scaled down from 322.7l to 4.5l
- Changed the ingredients to stuff I can easily buy


## History

### 2021-01-26
#### Brewing / fermentation:
- Put in lactose at 60m, before hops
- Colour looked a bit more brown than black while cooling - check it out at the end.. maybe consider 10% extra of each of the specialty grains
- OG came out at 1.046, and had 600ml extra
	- Removed 830ml from the pre-boil water
- Because we had extra wort, split it between the 5l demijohn and 1l squeeler
	- Followed yeast directions for demijohn, put a bit under 1g into squeeler (with maybe 700ml wort)
#### Bottling:
#### Tasting:
