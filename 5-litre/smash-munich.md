# Munich Smash

### Grains
0.76kg munich malt

### Water
4.5l end  
7.45l pre-boil  

- 4.5l \+ 0.836l grain absorbtion \+ 2.12l (32%) evaporation

### Mash
5.45l at 154f for 60m

### Sparge
2l at 170f for 15m

### Hops
15.5g tettnang hops

### Boil
60m  
13.5g tettnang hops at 60m to go  
2g tettnang hops at 1m to go

### Goal OG
1.046

### Yeast
1/5 packet safale american ale yeast US-05

### Goal pitch temp
20c

### Primary
19c

### Goal FG
1.0??

### Secondary
2.4 volumes

### Source
https://www.youtube.com/watch?v=C0s4cS_m5JM


## History

### 202x-xx-xx
#### Brewing / fermentation:
- Came in slightly under the water amounts.. added extra water for next time
- Some ice fell in the wort while it was cooling
### Bottling:
- Went pretty smoothely
- Nice taste if a bit bitter
### Tasting:
- Very sweet on the nose, and a tiny bit too sweet to taste
	- Added extra hops in at end of the boil
- Too flat - so added a higher carbonation volume (although I didn't write down the original volume so I just guessed..)
