# IP-Enrique

### Grains
1.25kg Pilsner  
0.45kg Caramunich / Caramalt

### Water
4.5l end  
8.5l pre-boil  

- 4.5l \+ 1.87l grain absorbtion \+ 3.75l evaporation

### Mash
6.5l at 162f for 60m

### Sparge
2l at 170f for 15m 

### Hops
32.3g Cascade Hops

### Other
1/3 Whirlfloc tablet

### Boil
60m total  
4.2g hops at 60m to go  
5.6g hops at 20m to go  
Whirlfloc at 10m to go  
8.5g hops at 1m to go

### Goal OG
1.075

### Yeast
A bit over 1/5 packet (3g) Safale American Ale Yeast US-05

### Goal pitch temp
20c

### Primary
18c
14g hops at 7 days to go

### Goal FG
1.013

### Secondary
2.3 volumes

### Source
https://www.brewersfriend.com/homebrew/recipe/view/234188/simple-cascade-ipa-one-gallon

### Notes
- Gave the beer a better name


## History

### 2020-12-13
#### Brewing / fermentation:
- chucked a bit too much campden tablet in (put a whole one in sparge water)
- came in short at 4l, upped the water by 1l
- 1/5 packet yeast might be fine.. used some calculator online to try a bit over
#### Bottling:
- one bottle ended up with some air, and another trube
- got 2x500ml and 5x330ml
#### Tasting:
- smells and tastes amazing
	- clean but with a full body.. really fruity
- a tiny bit too bubbly for my taste downed the volumes by 0.1
