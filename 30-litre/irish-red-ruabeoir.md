# Big Raubeoir

### Grains
4.53kg Light LME  

- 3.4kg light LME and 1.78kg Marris Otter Malts  

210g Caramunich  
210g Dark Crystal  
210g Roasted Barley

### Water
28l total  
18l cold  
10l post-boil  
14.4l pre-boil  

- 10l \+ 2.65l grain absorption \+ 1.76l (15%) evaporation

### Mash
10l at 153f for 60m

### Sparge
4.4l at 170f for 15m

### Pre-boil LME
240g light LME

### Target pre-boil gravity
1.046

### Hops
43g Goldings

### Boil
60m  
hops at 60m

### Post-boil LME
3.16kg light LME

### Goal OG
1.054

### Yeast
Safale US-05

### Goal pitch temp
20c

### Primary
19c

### Goal FG
1.014

### Secondary
2.3 volumes

### Source
Brewing Classic Styles - Jamil Zainasheff, John Palmer

### Notes
- Scaled up from 22.7l to 28l
- Converted to use ingredients I can easily buy

## History

### 2021-01-17
#### Brewing / fermentation:
- OG came in at 1.050 or 1.052.. so a tiny bit under
- cooled wort to 36 deg c before adding to water - ended up 22 deg c
- added 13l cold water then wort, then topped up with water
	- worked well the LME pushed the wort up to 12.5l
- used giant funnel with filter to get rid of trub as the grain bag split at some point
	- no grains in trub anyway
### Bottling:
### Tasting:
