# Mczainasheff's Big Wee

### Grains
8.1kg light LME  
0.56kg crystal malt  
280g munich malt  
280g amber malt  
139g dark crystal malt  
139g chocolate malt  

### Water
28l total  

- 13.9l cold  
- 4.09l post boil LME
- 10l post-boil  

11.77l pre-boil  

- 10l \+ 1.54l grain absorption \+ 1.76l (15% per hour) evaporation \- 1.53l LME

### Mash
8l at 154f for 60m

### Sparge
3.77l at 170f for 15m

### Pre-boil LME
2.2kg light LME

### Target pre-boil gravity
1.076

### Hops
73g Goldings

### Boil
60m  
56g hops at 60m  
17g hops at 10m  

### Post-boil LME
5.9kg light LME

### Goal OG
1.099

### Yeast
22.2g Safale US-05 (use two 11.5g packets)

### Goal pitch temp
20c

### Primary
19c

### Goal FG
1.026

### Secondary
2.3 volumes

### Source
Brewing Classic Styles - Jamil Zainasheff, John Palmer

### Notes
- Scaled up from 22.7l to 28l
- Converted to use ingredients I can easily buy

## History

### 202x-xx-xx
#### Brewing / fermentation:
### Bottling:
### Tasting:
