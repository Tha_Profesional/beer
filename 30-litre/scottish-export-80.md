# Scottish Export 80/large

### Grains
3.17kg light LME  
0.56kg crystal malt  
280g munich malt  
280g amber malt  
139g dark crystal malt  
104g chocolate malt  

### Water
28l total  

- 16.12l cold  
- 1.88l post boil LME
- 10l post-boil  

12.93l pre-boil  

- 10l \+ 1.5l grain absorption \+ 1.76l (15%) evaporation \- 0.33l LME

### Mash
8l at 158f for 60m

### Sparge
4.93l at 170f for 15m

### Pre-boil LME
.47kg light LME

### Target pre-boil gravity
1.0??

### Hops
34.5g Goldings

### Boil
60m  
hops at 60m

### Post-boil LME
2.7kg light LME

### Goal OG
1.050

### Yeast
12.3g Safale US-05 (use one 11.5g packet)

### Goal pitch temp
20c

### Primary
19c

### Goal FG
1.014

### Secondary
2 volumes

### Source
Brewing Classic Styles - Jamil Zainasheff, John Palmer

### Notes
- Scaled up from 22.7l to 28l
- Converted to use ingredients I can easily buy

## History

### 202x-xx-xx
#### Brewing / fermentation:
### Bottling:
### Tasting:
