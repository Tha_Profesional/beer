**2020-11-22** brewed 5-litre/smash-munich
**2020-12-06** bottled 5-litre/smash-munich
**2020-12-13** brewed 5-litre/ipa-enrique  
**2020-12-20** drank first 5-litre/smash-munich
**2020-12-21** brewed 3-litre/ginger-beer-spicey  
**2020-12-27** bottled 5-litre/ipa-enrique  
**2021-01-02** brewed 5-litre/weissbier-harold  
**2021-01-04** bottled 3-litre/ginger-beer-spicey  
**2021-01-10** drank first 5-litre/ipa-enrique  
**2021-01-16** bottled 5-litre/weissbier-harold  
**2021-01-17** brewed 30-litre/irish-red-ruabeoir
**2021-01-26** brewed 5-litre/sweet-stout-double-x
