# General Notes
- Folders are fermenter size, and make slightly less
	- 3l = 2.8l wort
	- 5l = 4.5l wort
	- 30l = 28l wort
- Grains absorb 1.1l for every 1kg
- Apparently evaporation is 15% per hour...
	- For 5l batches I've seen 32%
	- For 30l batches been using 15%
- Apparently 6lbs (2.72kg) of LME is 0.5 gallons (1.89l).. so to figure our LME volume use:  
	- 1.89l of water * (LME volume / 2.72)

