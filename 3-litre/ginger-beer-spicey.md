# That's-a-spicey (ginger) beer

### Grains
280g pilsner malt  
40g crystal malt  

### Water
2.8l end  
3.75l pre-boil  

- 2.8l \+ 0.352l grain absorbtion \+ 0.598l (18%) evaporation

### Mash
2.75l at 162f for 60m

### Sparge
1l at 170f for 15m  

### Boil ingredients
200g grated ginger  
50g grated fresh tumeric  
1 stick casia bark 
zest of 1 lemon / cut up fresh lemongrass  
150g raw sugar  

### Boil
20m  
180g ginger at 20m  
20g ginger at 1m  
150g sugar post-boil  

### Goal OG
1.046

### Yeast
1/3 packet (3g) Safale American Ale Yeast US-05

### Goal pitch temp
20c

### Primary
19c

### Goal FG
1.0??

### Secondary
3.0 volumes

### Source
https://www.thehopandgrain.com.au/brew-ginger-beer/

### Notes
- Scaled down from 4.5l to 2.8l


## History

### 2020-12-21
#### Brewing / fermentation:
- forgot the campden tablet pre-mash
- added a bit too much water, ended up with 100ml wort extra
- grain bag got scorched when boiling 
	- all the marks came off... but maybe next time just throw ingredients in and strain the wort
- split between growler and squeeler, with 1.5g yeast growler and 2.5g yeast squeeler
### Bottling:
- got 6 small bottles and 2 450ml bottles
- accidentally spilled maybe a shot of starsan into bottling bucket once beer was in there
- very dry and very spicey
### Tasting:
- nose is spot on - smells dry and ginger-y
- mouthfeel and fermentation are good.. not too spicey.. could be more ginger-y.. not sweet enough makes it a bit odd
	- changed 40g of pilsner malt into crystal malt to add some sugars
	- also upped mashing temperature slightly
