# Name

### Grains

### Water
xl end  
xl pre-boil  

- xl \+ xl grain absorbtion \+ xl (X%) evaporation

### Mash
xl at xxxf for xm

### Sparge
xl at xxxf for xm

### Hops

### Boil
xxm

### Goal OG
1.0xx

### Yeast

### Goal pitch temp
xxc

### Primary
xxc

### Goal FG
1.0xx

### Secondary
x.x volumes

### Source

### Notes


## History

### 202x-xx-xx
#### Brewing / fermentation:
#### Bottling:
#### Tasting:
